import QtQuick 2.0
import QtQuick.Controls 2.15

CommonMode {

    id: root

    width: 640
    height: width / 1024 * 600

    Column {

        id: midColumn

        width: parent.width / 2
        height: parent.height

        anchors.horizontalCenter: parent.horizontalCenter

        Item {

            id: topCont

            width: parent.width
            height: parent.height * 0.2
        }
        Item {

            id: midCont

            width: parent.width
            height: parent.height * 0.55

            Text {
                text: if (manager)
                          manager.setSpeed

                color: "white"
            }
        }
        Item {

            id: botCont

            width: parent.width
            height: parent.height * 0.25

            Item {

                id: btnContStart

                //                width: Math.min(parent.height, parent.width)
                width: parent.width * 0.55 / 2
                height: parent.height

                anchors.right: parent.horizontalCenter
                RoundButton {

                    id: startBtn

                    width: Math.min(parent.height, parent.width)
                    height: width

                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: if (manager)
                                   manager.start()
                }
            }
            Item {

                id: btnContStop

                //                width: Math.min(parent.height, parent.width)
                width: parent.width * 0.55 / 2
                height: parent.height

                anchors.left: parent.horizontalCenter
                RoundButton {

                    id: stopBtn

                    width: Math.min(parent.height, parent.width)
                    height: width

                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: if (manager)
                                   manager.stop()
                }
            }
        }
    }
    Column {

        id: leftColumn

        width: parent.width / 4
        height: parent.height

        anchors.right: midColumn.left

        Column {
            id: btnColumn
            width: parent.width * 0.75

            height: parent.height

            anchors.right: parent.right

            //            anchors.verticalCenter: parent.verticalCenter
            CustomButton {
                id: custBtnCont1

                width: parent.width
                height: parent.height / 4
            }
            CustomButton {
                id: custBtnCont2

                width: parent.width
                height: parent.height / 4
            }
            CustomButton {
                id: custBtnCont3

                width: parent.width
                height: parent.height / 4

                onClicked: if (manager)
                               manager.incSpeed()
            }
            CustomButton {
                id: custBtnCont4

                width: parent.width
                height: parent.height / 4
                onClicked: if (manager)
                               manager.decSpeed()
            }
        }
    }
    Column {

        id: rightColumn

        width: parent.width / 4
        height: parent.height

        anchors.left: midColumn.right
    }
}
