#include "pagemanager.h"


#include <QTimer>

#include "motormanager.h"

PageManager::PageManager(QObject *parent)
    : QObject(parent)
{
    m_topMotor = new MotorManager(this);
    m_botMotor = new MotorManager(this);

    m_currentMotor = Top;

    m_inProccess = false;
}

PageManager::~PageManager()
{

}

void PageManager::start()
{
    if(m_currentMotor & Motor::Top && m_topMotor){

        m_topMotor->start();
    }

    if(m_currentMotor & Motor::Bottom && m_botMotor){

        m_botMotor->start();
    }
}
void PageManager::stop()
{

    emit proccessChanged(m_inProccess = true);

    if(m_currentMotor & Motor::Top && m_topMotor){

        m_topMotor->stop();
    }

    if(m_currentMotor & Motor::Bottom && m_botMotor){

        m_botMotor->stop();
    }


    QTimer::singleShot(1000, [this](){

        emit proccessChanged(m_inProccess = false);

    });

}
void PageManager::incSpeed()
{
    if(m_currentMotor & Motor::Top && m_topMotor){

        m_topMotor->changeSpeed(1);

    }

    if(m_currentMotor & Motor::Bottom && m_botMotor){

        m_botMotor->changeSpeed(1);
    }


    emit setSpeedChanged(++m_setSpeed);
}
void PageManager::incPower()
{

}
void PageManager::decSpeed()
{
    if(m_currentMotor & Motor::Top && m_topMotor){

        m_topMotor->changeSpeed(-1);
    }

    if(m_currentMotor & Motor::Bottom && m_botMotor){

        m_botMotor->changeSpeed(-1);
    }

    emit setSpeedChanged(--m_setSpeed);
}
void PageManager::decPower()
{

}

void PageManager::changeDirection()
{

}

void PageManager::changeMotor()
{

}

bool PageManager::inProccess() const
{
    return m_inProccess;
}
