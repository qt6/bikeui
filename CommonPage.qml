import QtQuick 2.0
import QtQuick.Controls 2.15

import Manager 1.1

Page {

    id: root

    property PageManager manager: nullptr

    enabled: if (manager)
                 !manager.inProccess
}
