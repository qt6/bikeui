#ifndef MOTORMANAGER_H
#define MOTORMANAGER_H

#include <QObject>

class MotorManager : public QObject
{
    Q_OBJECT
public:
    explicit MotorManager(QObject *parent = nullptr);


    void start();
    void stop();

    void setMinSpeed(ushort val);
    void setMaxSpeed(ushort val);
    void setRangeSpeed(ushort minVal, ushort maxVal);
    void changeSpeed(short delta);


private:

    virtual void timerEvent(QTimerEvent *e) override;


signals:

private:

    int m_timerId;

    ushort m_setSpeed;
    ushort m_maxSpeed;
    ushort m_minSpeed;

};

#endif // MOTORMANAGER_H
