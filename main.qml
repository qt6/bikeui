import QtQuick 2.15
import QtQuick.Controls 2.15

import Manager 1.1

ApplicationWindow {
    width: 640
    height: width / 1024 * 600
    visible: true
    title: qsTr("Tabs")

    PageManager {
        id: pageManager

        onProccessChanged: isProccess => {
                               if (isProccess)
                               popup.open()
                           }
    }

    Popup {
        id: popup
        x: 100
        y: 100
        width: 200
        height: 300
        modal: true
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent

        ManualMode {
            id: manualMode

            manager: pageManager
        }
    }
}
