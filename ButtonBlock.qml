import QtQuick 2.0
import QtQuick.Controls 2.15

Column {

    id: root

    property bool isMirror: false

    width: 640
    height: width / 1024 * 600

    RoundButton {
        id: topBtn

        width: Math.min(parent.width * 2 / 3, parent.height / 2)
        height: width
        visible: true

        anchors.right: if (isMirror)
                           parent.right

        anchors.left: if (!isMirror)
                          parent.left
    }
    RoundButton {
        id: botBtn

        width: Math.min(parent.width * 2 / 3, parent.height / 2)
        height: width

        anchors.right: if (isMirror)
                           topBtn.horizontalCenter

        anchors.left: if (!isMirror)
                          topBtn.horizontalCenter
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.5;height:480;width:640}
}
##^##*/

