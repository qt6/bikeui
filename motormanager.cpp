#include "motormanager.h"

MotorManager::MotorManager(QObject *parent)
    : QObject(parent)
{
    m_timerId = 0;
    m_setSpeed = 0;
    m_minSpeed = 0;
    m_maxSpeed = 250;
}

void MotorManager::start()
{
    m_timerId = startTimer(200);
}

void MotorManager::stop()
{
    killTimer(m_timerId);

    m_timerId = 0;
}

void MotorManager::setMinSpeed(ushort val)
{
    m_minSpeed = val;
}

void MotorManager::setMaxSpeed(ushort val)
{
    m_maxSpeed = val;
}

void MotorManager::setRangeSpeed(ushort minVal, ushort maxVal)
{
    setMinSpeed(minVal);
    setMaxSpeed(maxVal);
}

void MotorManager::changeSpeed(short delta)
{
    if(delta > 0){
        if(m_setSpeed >= m_maxSpeed)
            return;
    }else if(delta < 0){
        if(m_setSpeed <= m_minSpeed)
            return;
    }


    m_setSpeed += delta;
}

void MotorManager::timerEvent(QTimerEvent *e)
{

}
