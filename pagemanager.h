#ifndef PAGEMANAGER_H
#define PAGEMANAGER_H

#include <QObject>

class MotorManager;

class PageManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool inProccess MEMBER m_inProccess NOTIFY proccessChanged)

    Q_PROPERTY(ushort setSpeed MEMBER m_setSpeed NOTIFY setSpeedChanged)
public:

    enum Direction{
        Forward = 0x01,
        Backward
    };

    enum Motor{
        Top = 0x1,
        Bottom = 0x2,
        Both = Top | Bottom
    };


    explicit PageManager(QObject *parent = nullptr);
    virtual ~PageManager() override;

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void incSpeed();
    Q_INVOKABLE void incPower();
    Q_INVOKABLE void decSpeed();
    Q_INVOKABLE void decPower();

    Q_INVOKABLE void changeDirection();
    Q_INVOKABLE void changeMotor();

    bool inProccess() const;

signals:

    void proccessChanged(bool status);
    void setSpeedChanged(ushort val);

private:

    MotorManager *m_topMotor;
    MotorManager *m_botMotor;


    Motor m_currentMotor;


    bool m_inProccess;

    ushort m_topRealSpeed;
    ushort m_setSpeed;
    ushort m_botRealSpeed;

};

#endif // PAGEMANAGER_H
