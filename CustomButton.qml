import QtQuick 2.0
import QtQuick.Controls 2.15

Column {
    id: root

    width: parent.width
    height: parent.height / 4

    signal clicked

    Item {
        width: parent.width
        height: parent.height * 0.9
        RoundButton {
            width: Math.min(parent.width, parent.height)
            height: width
            anchors.centerIn: parent

            onClicked: root.clicked()
        }
    }
    Item {
        width: parent.width
        height: parent.height * 0.1
        Text {

            anchors.centerIn: parent

            font.pixelSize: parent.height / 2
            text: "Some text"
        }
    }
}
